import javafx.util.Pair;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

public class Algo {

    public Map<Integer, Integer> printSumPairs(List input, int k) {
        Map<Integer, Integer> pairs = new HashMap<Integer, Integer>();
        for (int i = 0; i < input.size(); i++) {
            if (!pairs.containsKey(input.get(i))&& input.contains(k - (int) input.get(i))) {
                pairs.put(k - (int) input.get(i), (int) input.get(i));
            }
        }
            return pairs;
    }
}

