import javafx.util.Pair;
import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

import java.util.*;


public class TestAlgo {
  @Test
    public void Should_return_10(){
      Algo algo = new Algo();
      List<Integer> given = Arrays.asList(1,3,7,2,8,9,5,6);
      Map result =  algo.printSumPairs(given,9);
      Map<Integer,Integer> actual = new HashMap(){{put(2,7);put(8,1);put(6,3);}};
      Assert.assertThat(actual,is(result));
    }


  }
